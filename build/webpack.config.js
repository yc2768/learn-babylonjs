const path = require('path')
const webpack=require('webpack')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const isProd = process.env.NODE_ENV === 'production' // 是否生产环境

module.exports = function(options) {
  return {
    mode: isProd ? 'production' : 'development',
    entry: {
      main: path.resolve('src/index.ts')
    },
    output: {
      path: path.resolve(__dirname, '../dist'),
      filename: '[name].[contenthash:8].js'
    },
    devtool: isProd ? 'cheap-module-source-map' : 'cheap-module-eval-source-map',
    devServer: {
      disableHostCheck: true,
      host: 'localhost', // 主机名
      stats: 'errors-only', // 打包日志输出输出错误信息
      port: 8081,
      open: true
    },

    module: {
      rules: [
        { test: /\.ts$/, loader: "ts-loader" },
        { test: /\.css$/, loader: ExtractTextPlugin.extract({ fallback: "style-loader", use: "css-loader" }) },
        { test: /\.(woff|woff2|eot|ttf|svg)$/, loader: 'file-loader?limit=1024&name=[name].[ext]' },
      ]
    },

    plugins: [
      new CleanWebpackPlugin({}),
      new CopyWebpackPlugin({
        patterns: [{
                from: path.resolve(__dirname, '../static'),
                to:path.resolve(__dirname, '../dist/static'),
            }],
      }),
      new ExtractTextPlugin("styles.css"),
      new webpack.ProvidePlugin({
        'earcut': 'earcut'
      }),
      new HtmlWebpackPlugin({
        template: path.resolve("src", "index.html"),
        minify:{
          removeRedundantAttributes:true, // 删除多余的属性
          collapseWhitespace:true, // 折叠空白区域
          removeAttributeQuotes: true, // 移除属性的引号
          removeComments: true, // 移除注释
          collapseBooleanAttributes: true // 省略只有 boolean 值的属性值 例如：readonly checked
        },
      }),
    ],

    // Disable MAX 250kb entrypoint warnings on console
    performance: { hints: false },
    resolve: {
      extensions: ['.ts', '.js', '.json']
    }
  }
};
