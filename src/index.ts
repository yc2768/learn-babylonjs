import "./index.css";

//静态方法测试
// import  Playground  from './test/TestCode'
// const canvas = document.getElementById('renderCanvas') as HTMLCanvasElement;
// // Playground.CreateSphereScene(canvas);
// // Playground.CreateCube(canvas);
// // Playground.TestImportMeshAsync(canvas);
// // Playground.CreateVillage(canvas);
// Playground.CreateSmallCar(canvas);



//对象方式创建测试
import Playground from "./test/TestCode1";
let playground=new Playground();
// playground.DoRender(true, playground.CreateCube);
// playground.DoRender(false, playground.CreateDistantHills);
// playground.DoRender(false,playground.TestCreateLathe);
// playground.DoRender(false,playground.TestCreatelampLight);
// playground.DoRender(false,playground.TestCreateAnimatingCharacters);
// playground.DoRender(false,playground.TestAnimateWaitAsync);
// playground.DoRender(false,playground.TestSound);
playground.DoRender(false,playground.TestPointerDragBehavior);






