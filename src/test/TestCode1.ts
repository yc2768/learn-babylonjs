import * as BABYLON from 'babylonjs'
import * as GUI from 'babylonjs-gui'
import {StandardMaterial} from "babylonjs/Materials/standardMaterial";

export default class Playground {

    engine:BABYLON.Engine=null;
    scene:BABYLON.Scene=null;
    canvas:HTMLCanvasElement=null;
    static playGroundUrl="https://playground.babylonjs.com/";
    constructor() {
        this.canvas = document.getElementById('renderCanvas') as HTMLCanvasElement;
        //创建引擎
        this.engine=new BABYLON.Engine(this.canvas,true);
        //创建场景
        this.scene=new BABYLON.Scene(this.engine);
    }


    public  DoRender(useDefault:boolean, callBack:Function):void{

        if(useDefault)
        {
            //创建相机
            let camera=new BABYLON.FreeCamera("camera1",new BABYLON.Vector3(0,5,-10),this.scene);
            camera.setTarget(BABYLON.Vector3.Zero());
            camera.attachControl(this.canvas,true);
            //创建灯光
            let light=new BABYLON.HemisphericLight("light1",new BABYLON.Vector3(0,1,0),this.scene);
            light.intensity=0.7;
        }

        callBack.call(this);

        //渲染
        this.engine.runRenderLoop(()=>{
            if(this.scene&&this.scene.activeCamera)
            {
                this.scene.render();
            }
        });

        window.addEventListener("resize",()=>{
            this.engine.resize();
        });
    }


    /**
     * 创建Cube
     * @constructor
     */
    public   CreateCube(){

        const box=BABYLON.MeshBuilder.CreateBox("box",{width:0.5,height:0.5,depth:0.5});
    }


    public  CreateDistantHills():void{

        const camera = new BABYLON.ArcRotateCamera("camera", -Math.PI / 2, Math.PI / 2.5, 15, new BABYLON.Vector3(0, 0, 0),this.scene);
        camera.upperBetaLimit = Math.PI / 2.2;
        camera.attachControl(this.canvas, true);
        const light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(4, 1, 0),this.scene);

        const groundMat = new BABYLON.StandardMaterial("groundMat",this.scene);
        groundMat.diffuseTexture = new BABYLON.Texture("https://assets.babylonjs.com/environments/villagegreen.png",this.scene);
        groundMat.diffuseTexture.hasAlpha = true;

        const ground = BABYLON.MeshBuilder.CreateGround("ground", {width:24, height:24});
        ground.material = groundMat;


        const largeGroundMat = new BABYLON.StandardMaterial("largeGroundMat",this.scene);
        largeGroundMat.diffuseTexture = new BABYLON.Texture("https://assets.babylonjs.com/environments/valleygrass.png",this.scene);

        const largeGround = BABYLON.MeshBuilder.CreateGroundFromHeightMap("largeGround", "https://assets.babylonjs.com/environments/villageheightmap.png", {width:150, height:150, subdivisions: 20, minHeight:0, maxHeight: 10});
        largeGround.material = largeGroundMat;
        largeGround.position.y = -0.01;

        //Tree
        const spriteManagerTrees=new BABYLON.SpriteManager("treeManager","https://playground.babylonjs.com/textures/palm.png",2000,{width:512,height:1024},this.scene);
        for (let i = 0; i < 500; i++) {
            const tree=new BABYLON.Sprite("tree",spriteManagerTrees);
            tree.position.x=Math.random()*(-30);
            tree.position.z=Math.random()*20+8;
            tree.position.y=0.5;
        }


        const spriteManagerUFO = new BABYLON.SpriteManager("UFOManager","https://assets.babylonjs.com/environments/ufo.png", 1, {width: 128, height: 76},this.scene);
        const ufo = new BABYLON.Sprite("ufo", spriteManagerUFO);
        ufo.playAnimation(0, 16, true, 125);
        ufo.position.y = 5;
        ufo.position.z = 0;
        ufo.width = 2;
        ufo.height = 1;


        const fountainOutline = [
            new BABYLON.Vector3(0, 0, 0),
            new BABYLON.Vector3(0.5, 0, 0),
            new BABYLON.Vector3(0.5, 0.2, 0),
            new BABYLON.Vector3(0.4, 0.2, 0),
            new BABYLON.Vector3(0.4, 0.05, 0),
            new BABYLON.Vector3(0.05, 0.1, 0),
            new BABYLON.Vector3(0.05, 0.8, 0),
            new BABYLON.Vector3(0.15, 0.9, 0)
        ];

        //Create lathed fountain
        const fountain = BABYLON.MeshBuilder.CreateLathe("fountain", {shape: fountainOutline, sideOrientation: BABYLON.Mesh.DOUBLESIDE});
        fountain.position.x = -4;
        fountain.position.z = -6;



        //Skybox
        const skybox = BABYLON.MeshBuilder.CreateBox("skyBox", {size:150}, this.scene);
        const skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.scene);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("../../static/textures/skybox", this.scene);
        skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
        skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        skybox.material = skyboxMaterial;


    }

    public   TestCreateLathe(){

        const camera = new BABYLON.ArcRotateCamera("Camera", 3 * Math.PI / 2, Math.PI / 2.5, 70, BABYLON.Vector3.Zero(),this.scene);
        camera.attachControl(this.canvas, true);

        const light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 1, 0),this.scene);
        const fountainProfile = [
            new BABYLON.Vector3(0, 0, 0),
            new BABYLON.Vector3(10, 0, 0),
            new BABYLON.Vector3(10, 4, 0),
            new BABYLON.Vector3(8, 4, 0),
            new BABYLON.Vector3(8, 1, 0),
            new BABYLON.Vector3(1, 2, 0),
            new BABYLON.Vector3(1, 15, 0),
            new BABYLON.Vector3(3, 17, 0)
        ];

        //Create lathe
        const fountain = BABYLON.MeshBuilder.CreateLathe("fountain", {shape: fountainProfile, sideOrientation: BABYLON.Mesh.DOUBLESIDE}, this.scene);
        fountain.position.y=-5;

        var particleSystem=new BABYLON.ParticleSystem("particles",5000,this.scene);
        particleSystem.particleTexture=new BABYLON.Texture("https://playground.babylonjs.com/textures/flare.png",this.scene);
        particleSystem.emitter=new BABYLON.Vector3(0,10,0);
        particleSystem.minEmitBox=new BABYLON.Vector3(-1,0,0);
        particleSystem.maxEmitBox=new BABYLON.Vector3(1,0,0);
        particleSystem.color1=new BABYLON.Color4(0.7,0.8,1,1);
        particleSystem.color2=new BABYLON.Color4(0.2,0.5,1,1);
        particleSystem.colorDead=new BABYLON.Color4(0,0,0.2,0);
        particleSystem.minSize=0.1;
        particleSystem.maxSize=0.5;
        particleSystem.minLifeTime=2;
        particleSystem.maxLifeTime=3.5;
        particleSystem.emitRate=1500;
        particleSystem.blendMode=BABYLON.ParticleSystem.BLENDMODE_ONEONE;
        particleSystem.gravity=new BABYLON.Vector3(0,-9.81,0);
        particleSystem.direction1=new BABYLON.Vector3(-2,8,2);
        particleSystem.direction2=new BABYLON.Vector3(2,8,-2);
        particleSystem.minAngularSpeed=0;
        particleSystem.maxAngularSpeed=Math.PI;
        particleSystem.minEmitPower=1;
        particleSystem.maxEmitPower=3;
        particleSystem.updateSpeed=0.025;


        let switched=false;
        const pointerDown=(mesh)=>{
            if(mesh===fountain)
            {
                switched=!switched;
                if(switched)
                {
                    particleSystem.start();
                }
                else
                {
                    particleSystem.stop();
                }
            }
        }


        this.scene.onPointerObservable.add(pointerInfo=>{
            switch (pointerInfo.type) {
                case BABYLON.PointerEventTypes.POINTERDOWN:
                    if(pointerInfo.pickInfo.hit)
                    {
                            pointerDown(pointerInfo.pickInfo.pickedMesh);
                    }
                    break;
            }
        })


    }


    public  TestCreatelampLight(){
        const camera=new BABYLON.ArcRotateCamera('Camera',3*Math.PI/2,Math.PI/2,50,BABYLON.Vector3.Zero(),this.scene);
        camera.attachControl(this.canvas, true);
        const light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 50, 0),this.scene);
        light.intensity = 0;



        // GUI
        const adt = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");

        const panel = new GUI.StackPanel();
        panel.width = "220px";
        panel.top = "-25px";
        panel.horizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
        panel.verticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_BOTTOM;
        adt.addControl(panel);

        const header = new GUI.TextBlock();
        header.text = "Night to Day";
        header.height = "30px";
        header.color = "white";
        panel.addControl(header);

        const slider = new GUI.Slider();
        slider.minimum = 0;
        slider.maximum = 1;
        slider.borderColor = "black";
        slider.color = "gray";
        slider.background = "white";
        slider.value = 1;
        slider.height = "20px";
        slider.width = "200px";
        slider.onValueChangedObservable.add((value) => {
            if (light) {
                light.intensity = value;
            }
        });
        panel.addControl(slider);





        //add a spotlight and later after a mesh lamp post and a bulb have been created
        //then will make the post a parent to the bulb and
        //the bulb to the parent
        const lampLight = new BABYLON.SpotLight("lampLight", BABYLON.Vector3.Zero(), new BABYLON.Vector3(0, -1, 0), Math.PI, 1, this.scene);
        lampLight.diffuse = BABYLON.Color3.Yellow();

        //shape to extrude
        const lampShape = [];
        for(let i = 0; i < 20; i++) {
            lampShape.push(new BABYLON.Vector3(Math.cos(i * Math.PI / 10), Math.sin(i * Math.PI / 10), 0));
        }
        lampShape.push(lampShape[0]); //close shape

        //extrusion path
        const lampPath = [];
        lampPath.push(new BABYLON.Vector3(0, 0, 0));
        lampPath.push(new BABYLON.Vector3(0, 10, 0));
        for(let i = 0; i < 20; i++) {
            lampPath.push(new BABYLON.Vector3(1 + Math.cos(Math.PI - i * Math.PI / 40), 10 + Math.sin(Math.PI - i * Math.PI / 40), 0));
        }
        lampPath.push(new BABYLON.Vector3(3, 11, 0));

        const yellowMat = new BABYLON.StandardMaterial("yellowMat",this.scene);
        yellowMat.emissiveColor = BABYLON.Color3.Yellow();

        //extrude lamp
        const lamp = BABYLON.MeshBuilder.ExtrudeShape("lamp", {cap: BABYLON.Mesh.CAP_END, shape: lampShape, path: lampPath, scale: 0.5});

        // Shadow generator
        const  Dirlight = new BABYLON.DirectionalLight("dir01", new BABYLON.Vector3(0, -1, 1), this.scene);
        Dirlight.position = new BABYLON.Vector3(0, 15, -30);
        const shadowGenerator = new BABYLON.ShadowGenerator(1024, Dirlight);
        shadowGenerator.addShadowCaster(lamp, true);

        //add bulb
        const bulb = BABYLON.MeshBuilder.CreateSphere("bulb", {diameterX: 1.5, diameterZ: 0.8});

        bulb.material = yellowMat;
        bulb.parent = lamp;
        bulb.position.x = 2;
        bulb.position.y = 10.5;

        lampLight.parent = bulb;

        const ground = BABYLON.MeshBuilder.CreateGround("ground", {width:50, height: 50})
        ground.receiveShadows = true;

    }


    public TestCreateAnimatingCharacters(){

        // Low Poly Character with Blender Tutorial of Grant Abbitt: https://www.youtube.com/user/mediagabbitt
        // Character animations by Mixamo: https://www.mixamo.com/

        this.engine.enableOfflineSupport = false;


        var camera1 = new BABYLON.ArcRotateCamera("camera1", Math.PI / 2, Math.PI / 4, 10, new BABYLON.Vector3(0, -5, 0), this.scene);
        this.scene.activeCamera = camera1;
        this.scene.activeCamera.attachControl(this.canvas, true);
        camera1.lowerRadiusLimit = 2;
        camera1.upperRadiusLimit = 10;
        camera1.wheelDeltaPercentage = 0.01;

        // Lights
        var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), this.scene);
        light.intensity = 0.6;
        light.specular = BABYLON.Color3.Black();

        var light2 = new BABYLON.DirectionalLight("dir01", new BABYLON.Vector3(0, -0.5, -1.0), this.scene);
        light2.position = new BABYLON.Vector3(0, 5, 5);

        // Skybox
        var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", { size: 1000.0 }, this.scene);
        var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", this.scene);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://playground.babylonjs.com/textures/skybox2", this.scene);
        skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
        skyboxMaterial.diffuseColor = BABYLON.Color3.Black()
        skyboxMaterial.specularColor = BABYLON.Color3.Black();
        skybox.material = skyboxMaterial;

        // GUI
        var advancedTexture = GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
        var instructions = new GUI.TextBlock();
        instructions.text = "Move w/ WASD keys, B for Samba, look with the mouse";
        instructions.color = "white";
        instructions.fontSize = 16;
        instructions.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT
        instructions.textVerticalAlignment = GUI.Control.VERTICAL_ALIGNMENT_BOTTOM
        advancedTexture.addControl(instructions);

        // Ground
        var ground = BABYLON.MeshBuilder.CreateGround("ground", { height: 50, width: 50, subdivisions: 4 }, this.scene);
        var groundMaterial = new BABYLON.StandardMaterial("groundMaterial", this.scene);
        groundMaterial.diffuseTexture = new BABYLON.Texture("https://playground.babylonjs.com/textures/wood.jpg", this.scene);
        // groundMaterial.diffuseTexture.uScale = 30;
        // groundMaterial.diffuseTexture.vScale = 30;
        groundMaterial.specularColor = new BABYLON.Color3(.1, .1, .1);
        ground.material = groundMaterial;

        // Keyboard events
        var inputMap = {};
        this.scene.actionManager = new BABYLON.ActionManager(this.scene);
        this.scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyDownTrigger, function (evt) {
            inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
        }));
        this.scene.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnKeyUpTrigger, function (evt) {
            inputMap[evt.sourceEvent.key] = evt.sourceEvent.type == "keydown";
        }));


        // Load hero character
        BABYLON.SceneLoader.ImportMesh("", "https://assets.babylonjs.com/meshes/", "HVGirl.glb", this.scene, function (newMeshes, particleSystems, skeletons, animationGroups) {
            var hero = newMeshes[0];

            //Scale the model down
            hero.scaling.scaleInPlace(0.1);

            //Lock camera on the character
            this.camera1.target = hero;

            //Hero character variables
            var heroSpeed = 0.03;
            var heroSpeedBackwards = 0.01;
            var heroRotationSpeed = 0.1;

            var animating = true;

            const walkAnim = this.scene.getAnimationGroupByName("Walking");
            const walkBackAnim = this.scene.getAnimationGroupByName("WalkingBack");
            const idleAnim = this.scene.getAnimationGroupByName("Idle");
            const sambaAnim = this.scene.getAnimationGroupByName("Samba");



            //Rendering loop (executed for everyframe)
            this.scene.onBeforeRenderObservable.add(() => {
                var keydown = false;
                //Manage the movements of the character (e.g. position, direction)
                if (inputMap["w"]) {
                    hero.moveWithCollisions(hero.forward.scaleInPlace(heroSpeed));
                    keydown = true;
                }
                if (inputMap["s"]) {
                    hero.moveWithCollisions(hero.forward.scaleInPlace(-heroSpeedBackwards));
                    keydown = true;
                }
                if (inputMap["a"]) {
                    hero.rotate(BABYLON.Vector3.Up(), -heroRotationSpeed);
                    keydown = true;
                }
                if (inputMap["d"]) {
                    hero.rotate(BABYLON.Vector3.Up(), heroRotationSpeed);
                    keydown = true;
                }
                if (inputMap["b"]) {
                    keydown = true;
                }

                //Manage animations to be played
                if (keydown) {
                    if (!animating) {
                        animating = true;
                        if (inputMap["s"]) {
                            //Walk backwards
                            walkBackAnim.start(true, 1.0, walkBackAnim.from, walkBackAnim.to, false);
                        }
                        else if
                        (inputMap["b"]) {
                            //Samba!
                            sambaAnim.start(true, 1.0, sambaAnim.from, sambaAnim.to, false);
                        }
                        else {
                            //Walk
                            walkAnim.start(true, 1.0, walkAnim.from, walkAnim.to, false);
                        }
                    }
                }
                else {

                    if (animating) {
                        //Default animation is idle when no key is down
                        idleAnim.start(true, 1.0, idleAnim.from, idleAnim.to, false);

                        //Stop all animations besides Idle Anim when no key is down
                        sambaAnim.stop();
                        walkAnim.stop();
                        walkBackAnim.stop();

                        //Ensure animation are played only once per rendering loop
                        animating = false;
                    }
                }
            });
        });


    }

    public  TestAnimateWaitAsync(){
        let ligth=new BABYLON.PointLight("Omni",new BABYLON.Vector3(0,100,100),this.scene);
        let camera=new BABYLON.ArcRotateCamera("Camera",0,0.8,100, BABYLON.Vector3.Zero(),this.scene);
        camera.attachControl(this.canvas,true);
        let box1=BABYLON.Mesh.CreateBox("Box1",10,this.scene);
        let materialBox=new BABYLON.StandardMaterial("mat",this.scene);
        materialBox.diffuseColor=new BABYLON.Color3(0,1,0);
        box1.material=materialBox;
        let animationBox=new BABYLON.Animation("tutoAnimation","scaling.x",30,BABYLON.Animation.ANIMATIONTYPE_FLOAT,BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
        let keys=[];
        keys.push({
           frame:0,
           value:1
        });

        keys.push({
           frame:20,
           value:0.2
        });

        keys.push({
           frame:100,
           value:1
        });

        animationBox.setKeys(keys);
        box1.animations.push(animationBox);

        // setTimeout( async ()=>{
        //
        //     let anim=this.scene.beginAnimation(box1,0,100,false);
        //     console.log("before");
        //     await anim.waitAsync();
        //     console.log("after");
        // });


        this.scene.beginAnimation(box1,0,100,true);
        let t=0;
        this.scene.onBeforeRenderObservable.add(()=>{
            t+=this.engine.getDeltaTime()*0.001;
            let d=Math.sign(t)*30;
        })









    }


    public  TestRegisterBeforeRender(){
        // This creates and positions a free camera (non-mesh)
        var camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(0, 5, -10), this.scene);

        // This targets the camera to scene origin
        camera.setTarget(BABYLON.Vector3.Zero());

        // This attaches the camera to the canvas
        camera.attachControl(this.canvas, true);

        // This creates a light, aiming 0,1,0 - to the sky (non-mesh)
        var light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), this.scene);

        // Default intensity is 1. Let's dim the light a small amount
        light.intensity = 0.7;

        // Our built-in 'sphere' shape.
        var box = BABYLON.MeshBuilder.CreateBox("box", {}, this.scene);

        // Set a direction flag for the animation
        var direction = true;

        this.scene.registerBeforeRender(function () {

            if(box.position.x<2&&direction){
                box.position.x+=0.05;
            }
            else
            {
                direction=false;
            }

            if(box.position.x>-2&&!direction)
            {
                box.position.x-=0.05;
            }
            else
            {
                direction=true;
            }


        })





    }


    public  TestSound(){

        //循环播放音乐
        // var music = new BABYLON.Sound("Violons", "https://playground.babylonjs.com/sounds/violons11.wav", this.scene, null, { loop: true, autoplay: true });

         //回调播放
         // let music=new BABYLON.Sound("Violons","https://playground.babylonjs.com/sounds/violons11.wav", this.scene,function () {
         //     music.play();
         // })

        //交互播放
        // let gunshot=new BABYLON.Sound("gunshot", "https://playground.babylonjs.com/sounds/gunshot.wav", this.scene);
        // window.addEventListener("keydown",function (evt) {
        //     if(evt.keyCode===32)
        //     {
        //         gunshot.play();
        //     }
        // })


        //播放一些列音乐
        // var music1 = new BABYLON.Sound("Violons11", Playground.playGroundUrl+"sounds/violons11.wav", this.scene,
        //     soundReady, { loop: true });
        // var music2 = new BABYLON.Sound("Violons18", Playground.playGroundUrl+"sounds/violons18.wav", this.scene,
        //     soundReady, { loop: true });
        // var music3 = new BABYLON.Sound("Cellolong", Playground.playGroundUrl+"sounds/cellolong.wav", this.scene,
        //     soundReady, { loop: true });
        // var soundsReady=0;
        // function soundReady() {
        //    soundsReady++;
        //    if(soundsReady===3)
        //    {
        //        music1.play();
        //        music2.play();
        //        music3.play();
        //    }
        // }
    }

    public  Test3DSound(){
        //播放3D音乐
        // Lights
        var light0 = new BABYLON.DirectionalLight("Omni", new BABYLON.Vector3(-2, -5, 2), this.scene);
        var light1 = new BABYLON.PointLight("Omni", new BABYLON.Vector3(2, -5, -2), this.scene);

        // Need a free camera for collisions
        var camera = new BABYLON.FreeCamera("FreeCamera", new BABYLON.Vector3(0, -8, -20), this.scene);
        camera.attachControl(this.canvas, true);

        //Ground
        var ground = BABYLON.Mesh.CreatePlane("ground", 400.0, this.scene);
        ground.material = new BABYLON.StandardMaterial("groundMat", this.scene);
        (ground.material as BABYLON.StandardMaterial).diffuseColor = new BABYLON.Color3(1, 1, 1);
        ground.material.backFaceCulling = false;
        ground.position = new BABYLON.Vector3(5, -10, -15);
        ground.rotation = new BABYLON.Vector3(Math.PI / 2, 0, 0);

        //Simple crate
        var box = BABYLON.Mesh.CreateBox("crate", 2, this.scene);
        box.material = new BABYLON.StandardMaterial("Mat", this.scene);
        (box.material as StandardMaterial).diffuseTexture = new BABYLON.Texture(Playground.playGroundUrl+"textures/crate.png", this.scene);
        box.position = new BABYLON.Vector3(10, -9, 0);

        var music=new BABYLON.Sound("Violons",Playground.playGroundUrl+"sounds/violons11.wav", this.scene,function () {



        },{loop:true,autoplay:true});
        music.attachToMesh(box);
        this.scene.gravity=new BABYLON.Vector3(0,-0.9,0);
        this.scene.collisionsEnabled=true;
        camera.checkCollisions=true;
        camera.applyGravity=true;
        camera.ellipsoid=new BABYLON.Vector3(1,1,1);
        ground.checkCollisions=true;

        var alpha=0;
        this.scene.registerBeforeRender(function () {
            box.position = new BABYLON.Vector3(Math.cos(alpha) * 30, -9, Math.sin(alpha) * 30);
            alpha += 0.01;
        })
    }



    public  TestPointerDragBehavior(){
        var camera = new BABYLON.FreeCamera("camera1", new BABYLON.Vector3(1, 5, -10), this.scene);
        camera.setTarget(BABYLON.Vector3.Zero());
        var light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), this.scene);
        light.intensity = 0.7;
        var sphere = BABYLON.Mesh.CreateSphere("sphere1", 16, 2, this.scene);
        sphere.rotation.x = Math.PI/2
        sphere.position.y = 1;
        var ground = BABYLON.Mesh.CreateGround("ground1", 6, 6, 2, this.scene);

        var pointerDragBehavior=new BABYLON.PointerDragBehavior({dragAxis:new BABYLON.Vector3(1,1,0)});
        pointerDragBehavior.useObjectOrientationForDragging=false;

        pointerDragBehavior.onDragStartObservable.add(function (evt) {

        })
        pointerDragBehavior.onDragObservable.add((event)=>{
            console.log("drag");
            console.log(event);
        })
        pointerDragBehavior.onDragEndObservable.add((event)=>{
            console.log("dragEnd");
            console.log(event);
        })

       sphere.addBehavior(pointerDragBehavior);

    }


}
